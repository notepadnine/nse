import argparse
from scrapy import cmdline
import datetime
import os

def run(recipients=None):
    run_date = datetime.datetime.today()
    command = "scrapy crawl nse"
    cmdline.execute(command.split())

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    run()