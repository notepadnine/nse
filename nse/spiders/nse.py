import scrapy
import os
import urllib2
import csv
import time


class QuotesSpider(scrapy.Spider):
    name = "nse"
    eq= []
    data_location_dir = 'equity_csv'
    equity_csv_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'nse.csv')
    equity_data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'csv-data')
    allowed_domains = ['nseindia.com']
    hdr = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
        'Accept-Encoding': 'none',
        'Accept-Language': 'en-US,en;q=0.8',
        'Connection': 'keep-alive'}
    # import pdb;
    # pdb.set_trace()


    def __init__(self, location=''):
        self.location = location
        if not os.path.exists(self.equity_csv_dir):
            try:
                os.makedirs(self.equity_data_dir)
                os.makedirs(self.equity_data_dir)
            except:
                pass
        self.eq = self.fetch_equity_list()
    def fetch_equity_list(self):
        # first download fresh list or stocks
        url = 'https://www.nseindia.com/content/equities/EQUITY_L.csv'
        req = urllib2.Request(url, headers=self.hdr)
        response = urllib2.urlopen(req)
        html = response.read()
        with open(self.equity_csv_dir, 'wb+') as f:
            f.write(html)
        # now read it
        symbol_list = []
        with open(self.equity_csv_dir, 'rb') as f:
            reader = csv.reader(f, delimiter=',')
            for row in reader:
                symbol_list.append(row[0])
        # return symbol_list[124:132]  # remove in production
        return symbol_list[1:]  # skip header

    def start_requests(self):

        TIME_SERIES_LIST = ['LAST_1_MONTH', 'LAST_6_MONTHS', 'LAST_1_YEAR',
                            'LAST_2_YEARS', 'LAST_5_YEARS', 'MORE_THAN_5_YEARS']
        CONST_URL = 'https://www.nseindia.com/corporates/datafiles/LDE_{}EQUITIES_{}.csv'

        start_urls = []
        for i in self.eq:
            for j in TIME_SERIES_LIST:
                start_urls.append(CONST_URL.format(str(i) + '_', j))

        for url in start_urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        if len(filter(None, response.body.split('\n'))) > 1:
            filename = os.path.join(self.equity_data_dir, os.path.split(response.url)[-1])
            # import pdb;pdb.set_trace()
            # filename = eq + time_limit

            with open(filename, 'wb') as f:
                f.write(response.body)
            self.log('Saved file %s' % filename)
        time.sleep(2)