import csv
import os
import time
import urllib2

data_location_dir = 'equity_csv'
equity_csv_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), data_location_dir)

hdr = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
    'Accept-Encoding': 'none',
    'Accept-Language': 'en-US,en;q=0.8',
    'Connection': 'keep-alive'}


def fetch_equity_list():
    if not os.path.exists(equity_csv_dir):
        try:
            os.makedirs(equity_csv_dir)
        except:
            return 'Permission error'

    # first download fresh list
    url = 'https://www.nseindia.com/content/equities/EQUITY_L.csv'
    req = urllib2.Request(url, headers=hdr)
    response = urllib2.urlopen(req)
    html = response.read()
    with open(os.path.join(equity_csv_dir, 'nse.csv'), 'wb+') as f:
        f.write(html)
    # now read it
    symbol_list = []
    with open(os.path.join(equity_csv_dir, 'nse.csv'), 'rb') as f:
        reader = csv.reader(f, delimiter=',')
        for row in reader:
            symbol_list.append(row[0])
    return symbol_list[23:28]  # remove in production
    return symbol_list[1:]  # skip header


def save_file(equity_code, url):
    req = urllib2.Request(url, headers=hdr)

    file_obj = open(os.path.join(equity_csv_dir, equity_code + ".csv"), "wb")
    writer = csv.writer(file_obj, delimiter=";")

    try:
        response = urllib2.urlopen(req)
        data = csv.reader(response)

        for i in data:
            if i[0] == 'Symbol': continue
            writer.writerow(i)
        file_obj.close()

    except urllib2.HTTPError as err:
        print url + ' is not valid'


def valid_url_test(url):
    return True


def download_data():
    CONST_URL = 'https://www.nseindia.com/corporates/datafiles/LDE_{}EQUITIES_{}.csv'
    TIME_SERIES_LIST = ['LAST_1_MONTH', 'LAST_6_MONTHS', 'LAST_1_YEAR',
                        'LAST_2_YEARS', 'LAST_5_YEARS', 'MORE_THAN_5_YEARS']
    EQ_LIST = fetch_equity_list()

    for eq in EQ_LIST:
        for time_limit in TIME_SERIES_LIST:
            eq_url = CONST_URL.format(str(eq) + '_', time_limit)
            if valid_url_test(eq_url) is True:
                save_file(eq + time_limit, eq_url)
                time.sleep(3)


def delete_empty_file():
    target_size = 0
    import os
    for dirpath, dirs, files in os.walk(equity_csv_dir):
        for file in files:
            path = os.path.join(dirpath, file)
            if os.stat(path).st_size == target_size:
                os.remove(path)
def execute():
    fetch_equity_list()
    download_data()
    delete_empty_file()


if __name__ == '__main__':
    execute()
